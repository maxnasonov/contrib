## List of files and theirs purpose

* [uninstall-x64.bat](#about-uninstall-x64bat)
* [convert-guid.py](#about-convert-guidpy)
* [reg-entries.reg](#about-reg-entriesreg)

## About uninstall-x64.bat

This is the main script, other files are auxiliary.

The script can be used to uninstall NXLog and remove traces of the installation.

The following files should be present in the current directory:
  * reg-entries.reg: auxiliary file, see below.
  * uninstall-x64.bat: main script
  * nxlog-*_windows_x64.msi: should be passed as the argument to the uninstall-x64.bat. It must have the same version which was used for the installation. For example, if the nxlog-4.6.4692_windows_x64.msi was used for the installation, then the nxlog-4.6.4692_windows_x64.msi should be passed as the argument to uninstall-x64.bat.

### Example of usage

```sh
uninstall-x64.bat nxlog-4.6.4692_windows_x64.msi
```

### How it works?

1. The script calls `msiexec`, with the `nxlog-*_windows_x64.msi` file as its argument, the `msiexec` uses the [Windows Installer](https://docs.microsoft.com/en-us/windows/win32/msi/windows-installer-portal) feature to remove NXLog from the system. As a result, it will remove almost all of NXLog's files and registry entries.
2. The script calls `regedit.exe` with the `reg-entries.reg` file as its argument. As a result, it will remove the possible leftover registry entries after calling `msiexec` from the step 1.
3. The script asks for confirmation to delete the NXLog installation directory (`C:\Program Files\nxlog`) and in case of positive answer, it does that.

## About convert-guid.py

This is an auxiliary script and it is not used in the uninstall-x64.bat, it is only necessary to help with converting GUIDs present in the reg-entries.reg file.

It converts GUIDs taken from Windows registry to the format used in .wxs files and vice versa. The script accepts any number of GUIDs as arguments.

### Examples of usage

From registry format:
```sh
./convert-guid.py from-reg-format C1BCB91BF6950E114B3E02FC03A5ECE5 8C06BD22F7950E114B3E02FC03A5ECE5
C1BCB91BF6950E114B3E02FC03A5ECE5       # registry format
B19BCB1C-596F-11E0-B4E3-20CF305ACE5E   # normal (.wxs format)
{B19BCB1C-596F-11E0-B4E3-20CF305ACE5E} # normal (.wxs format), enclosed in `{`, `}`

8C06BD22F7950E114B3E02FC03A5ECE5       # registry format
22DB60C8-597F-11E0-B4E3-20CF305ACE5E   # normal (.wxs format)
{22DB60C8-597F-11E0-B4E3-20CF305ACE5E} # normal (.wxs format), enclosed in `{`, `}`

```

To registry format:
```sh
# GUIDs, passed as arguments, can be optionally enclosed in `{`, `}`, for example: `{88D16508-59FC-11E0-B4E3-20CF305ACE5E}`
 ./convert-guid.py to-reg-format B19BCB1C-596F-11E0-B4E3-20CF305ACE5E 620F97A6-59DD-11E0-B4E3-20CF305ACE5E '{22DB60C8-597F-11E0-B4E3-20CF305ACE5E}'
C1BCB91BF6950E114B3E02FC03A5ECE5       # registry format
B19BCB1C-596F-11E0-B4E3-20CF305ACE5E   # normal (.wxs format)
{B19BCB1C-596F-11E0-B4E3-20CF305ACE5E} # normal (.wxs format), enclosed in `{`, `}`

6A79F026DD950E114B3E02FC03A5ECE5       # registry format
620F97A6-59DD-11E0-B4E3-20CF305ACE5E   # normal (.wxs format)
{620F97A6-59DD-11E0-B4E3-20CF305ACE5E} # normal (.wxs format), enclosed in `{`, `}`

8C06BD22F7950E114B3E02FC03A5ECE5       # registry format
22DB60C8-597F-11E0-B4E3-20CF305ACE5E   # normal (.wxs format)
{22DB60C8-597F-11E0-B4E3-20CF305ACE5E} # normal (.wxs format), enclosed in `{`, `}`

```

## About reg-entries.reg

This is an auxiliary file and used in the uninstall-x64.bat to remove possibly left registry entries. 