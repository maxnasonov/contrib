@echo off
rem NB: this script can delete the `C:\Program Files\nxlog` recursively! Although this feature was tested in testing environment, it was not tested under all possible conditions. It is a safer to run commands from the uninstall-x64.bat manually. Also it may be more preferable to use specialized software (uninstallers).

setlocal enabledelayedexpansion

for %%a in (%*) do (
    goto START
)
goto INVALID_ARGS

:START

set MSI_FILE_PATH=%1
set INSTALL_DIR_PATH=C:\Program Files\nxlog
set REG_FILE_PATH=reg-entries.reg

if not exist %MSI_FILE_PATH% (
    echo ERROR: The file %MSI_FILE_PATH% does not exist.
    goto ERR
)
if not exist %REG_FILE_PATH% (
    echo ERROR: The file %REG_FILE_PATH% does not exist.
    goto ERR
)

set /p ANSWER="Uninstall the 'nxlog' (y/[n])? "
if /i "%ANSWER%" neq "y" goto END

msiexec /x %MSI_FILE_PATH% /q

regedit.exe /S %REG_FILE_PATH%

if exist "%INSTALL_DIR_PATH%" (
    set /p ANSWER1="Delete the '%INSTALL_DIR_PATH%' directory and all files in it (y/[n])? "
    rem `set` does not work properly in `if`, if `setlocal enabledelayedexpansion` is not set.
    if /i "!ANSWER1!" neq "y" goto END
    del /s /f /q "%INSTALL_DIR_PATH%" > NUL
    rmdir /s /q "%INSTALL_DIR_PATH%" > NUL
    if exist "%INSTALL_DIR_PATH%" rd /s /q "%INSTALL_DIR_PATH%" > NUL
)
setlocal disabledelayedexpansion
echo Done!
goto END

:INVALID_ARGS
echo ERROR: Invalid arguments.
echo Usage example: %0 nxlog-4.99.5432_windows_x64.msi

:ERR
endlocal
exit /b 1

:END
endlocal
