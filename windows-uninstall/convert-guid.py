#!/usr/bin/env python
"""
Script requires Python >= 3.5

Algorithm found in https://www.symantec.com/connect/articles/brief-note-installer-guids:
    The first 8 digits are reversed, the hyphen is dropped
    the next 4 digits are reversed, the hyphen is dropped
    the next 4 digits are reversed, the hyphen is dropped
    the next two digits are reversed
    the next two digits are reversed, the hyphen is dropped
    then the next two digits are reversed, six times. So a code such as {D0F23C3F-CA74-460F-9ADB-49CBD57F9688} becomes: F3C32F0D47ACF064A9BD94BC5DF76988.
"""
import sys


def reverse(s: str) -> str:
    return s[::-1]


def substr(s: str, offset: int, length: int) -> str:
    return s[offset:offset + length]


# For the 'F3C32F0D47ACF064A9BD94BC5DF76988' returns 'D0F23C3F-CA74-460F-9ADB-49CBD57F9688'
def from_reg_format(guid: str) -> str:
    return reverse(substr(guid, 0, 8)) \
           + '-' + reverse(substr(guid, 8, 4)) \
           + '-' + reverse(substr(guid, 12, 4)) \
           + '-' + reverse(substr(guid, 16, 2)) + reverse(substr(guid, 18, 2)) \
           + '-' + reverse(substr(guid, 20, 2)) + reverse(substr(guid, 22, 2)) + reverse(substr(guid, 24, 2)) + reverse(substr(guid, 26, 2)) + reverse(substr(guid, 28, 2)) + reverse(substr(guid, 30, 2))


# For the 'B19BCB1C-596F-11E0-B4E3-20CF305ACE5E' return 'C1BCB91BF6950E114B3E02FC03A5ECE5'
def to_reg_format(guid: str) -> str:
    return from_reg_format(remove_braces(guid.replace('-', ''))).replace('-', '')


def show_usage() -> None:
    print("Usage:\n "
          + "\t" + sys.argv[0] + " from-reg-format ...$guid\n\tExample: " + sys.argv[0] + " decode 5F829299CF35F4D46930B5A78DCA8B9A F3C32F0D47ACF064A9BD94BC5DF76988\n"
          + "\tor\n\t" + sys.argv[0] + " to-reg-format ...$guid\n\tExample: " + sys.argv[0] + " encode {B19BCB1C-596F-11E0-B4E3-20CF305ACE5E} D0F23C3F-CA74-460F-9ADB-49CBD57F9688")


def remove_braces(s: str) -> str:
    return s.replace('}', '').replace('{', '')


if __name__ == "__main__":
    if len(sys.argv) < 3:
        show_usage()
        sys.exit(1)
    action = sys.argv[1]
    args = sys.argv[2:]
    if action == 'to-reg-format':
        for guid in args:
            encoded_guid = to_reg_format(guid)
            print(encoded_guid + "\n" + remove_braces(guid) + "\n{" + remove_braces(guid) + "}\n")
    elif action == 'from-reg-format':
        for guid in args:
            decoded_guid = from_reg_format(guid)
            print(guid + "\n" + decoded_guid + "\n{" + decoded_guid + "}\n")
    else:
        show_usage()
        sys.exit(1)
