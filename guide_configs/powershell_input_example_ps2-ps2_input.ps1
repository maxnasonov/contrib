#Requires -Version 2

$count = 0

while($true) {
    $count += 1
    $now = [System.DateTime]::UtcNow

    # Set event fields
    $event = @{
        Arguments = [system.String]::Join(", ", $args);
        EventTime = $now.ToString('o');
        Message = "event$count";
    }

    # Return event as CSV
    $row = New-Object PSObject
    $event.GetEnumerator() | Sort-Object -Property Name | ForEach-Object {
        $row | Add-Member -MemberType NoteProperty -Name $_.Name -Value $_.Value
    }
    $row | ConvertTo-Csv -NoTypeInformation | Select-Object -Skip 1 | Write-Output

    Start-Sleep -Seconds 5
}