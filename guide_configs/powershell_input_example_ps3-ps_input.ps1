#Requires -Version 3

# Use this if you need 64-bit PowerShell (has no effect on 32-bit systems).
#if ($env:PROCESSOR_ARCHITEW6432 -eq "AMD64") {
#    Write-Debug "Running 64-bit PowerShell."
#    &"$env:SYSTEMROOT\SysNative\WindowsPowerShell\v1.0\powershell.exe" `
#    -NonInteractive -NoProfile -ExecutionPolicy Bypass `
#    -File "$($myInvocation.InvocationName)" $args
#    exit $LASTEXITCODE
#}

# Use this if you need 32-bit PowerShell.
#if ($env:PROCESSOR_ARCHITECTURE -ne "x86") {
#    Write-Debug "Running 32-bit PowerShell."
#    &"$env:SYSTEMROOT\SysWOW64\WindowsPowerShell\v1.0\powershell.exe" `
#    -NonInteractive -NoProfile -ExecutionPolicy Bypass `
#    -File "$($myInvocation.InvocationName)" $args
#    exit $LASTEXITCODE
#}

# The position is saved in this file for resuming.
$CacheFile = 'C:\nxlog_position_cache.txt'

# The source is queried at this interval in seconds.
$PollInterval = 1
################################################################################

# Get position from the cache file (on the first run, create the file)
# This example uses a timestamp position, but some sources may use an id, etc.
function Get-Position {
    param( $file )
    if (Test-Path $file) {
        # Read timestamp from file
        $time = (Get-Date (Get-Content $file -First 1))
        # Convert timestamp to UTC
        $time = $time.ToUniversalTime()
        # Round down to nearest second
        $time = $time.AddTicks(-($time.Ticks % 10000000))
    }
    else {
        # Get current time in UTC
        $time = [System.DateTime]::UtcNow
        # Round down to nearest second
        $time = $time.AddTicks(-($time.Ticks % 10000000))
        # Create position cache file with specified time
        Save-Position $file $time
    }
    return $time
}

# Save position to cache file
function Save-Position {
    param( $file, $time )
    Out-File -FilePath $file -InputObject $time.ToString('o')
}

# Main
Try {
    $start = Get-Position $CacheFile
    $count = 0
    $cpu = Get-WmiObject Win32_Processor | Select -First 1 -Expand datawidth
    $os = [int]::Parse(((gwmi Win32_OperatingSystem).OSArchitecture -Split "-")[0])
    $process = [IntPtr]::Size * 8

    # Repeatedly read from the source
    while($true) {
        $count += 1

        # Get current time in UTC and round down to the nearest second
        $now = [System.DateTime]::UtcNow
        $end = $now.AddTicks(-($now.Ticks % 10000000))

        # Set event fields
        $event = @{
            Arguments = [system.String]::Join(", ", $args);
            StartTime = $start.ToString('o');
            EndTime = $end.ToString('o');
            CPUBitness = $cpu;
            OSBitness = $os;
            ProcessBitness = $process;
            Message = "Test event $count for this run";
        }

        # Return event as JSON
        $event | ConvertTo-Json -Compress | Write-Output

        # Save position to cache file and sleep until the next iteration
        Save-Position $CacheFile $end
        Start-Sleep -Seconds $PollInterval
        $start = $end
    }
}
Catch {
    Write-Error "An unhandled exception occurred!"
    exit 1
}