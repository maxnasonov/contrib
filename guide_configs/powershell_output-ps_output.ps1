#Requires -Version 3

while($line = [Console]::In.ReadLine()) {
    # Convert from JSON
    $record = $line | ConvertFrom-Json

    # Write out to file
    $record | Out-File -FilePath 'C:\out.log' -Append
}