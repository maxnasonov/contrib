#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib $FindBin::Bin;
use Log::Nxlog;
use XML::LibXML;

sub read_data {
    my $doc = XML::LibXML->load_xml( location => 'scan.nessus' );
    my $report = $doc->findnodes('/NessusClientData_v2/Report');
    my @nessus_sev = ("INFO","LOW","MEDIUM","HIGH","CRITICAL");
    my @nxlog_sev_val = (2,3,4,5,5);
    my @nxlog_sev = ("INFO","WARNING","ERROR","CRITICAL","CRITICAL");
    my %mon2num = qw(
                      Jan 01  Feb 02  Mar 03  Apr 04 May 05 Jun 06
                      Jul 07  Aug 08  Sep 09  Oct 10 Nov 11 Dec 12
                     );
    my $eventtime;

    foreach my $reportHost ( $doc->findnodes('/NessusClientData_v2/Report/ReportHost') )
    {
        $eventtime = "";
        foreach my $properties ( $reportHost->findnodes('./HostProperties/tag') ) {
            if ($properties->getAttribute('name') eq "HOST_END") {
                my($dow, $m, $d, $t, $y) = $properties->textContent =~
                  m<^([a-zA-Z]+) ([a-zA-Z]+) (\d+) (\d+:\d+:\d+) (\d+)$> or die "Not a valid date";
               $eventtime= $y . "-" . $mon2num{$m} . "-" . $d . " " . $t;
            }
         }
        foreach my $reportItem ( $reportHost->findnodes('./ReportItem') ) {
            my $event = Log::Nxlog::logdata_new();
            my $raw_event = $reportItem->toString();
            Log::Nxlog::set_field_string( $event, 'raw_event', $raw_event );
            Log::Nxlog::set_field_string( $event, 'EventTime', $eventtime );
            Log::Nxlog::set_field_string( $event, 'Report', $report->get_node(1)->getAttribute('name') );
            Log::Nxlog::set_field_string( $event, 'ReportHost', $reportHost->getAttribute('name') );
            my @atts = $reportItem->getAttributes();
            foreach my $at (@atts) {
                my $na = $at->getName();
                my $va = "" . $at->getValue();
                if ($na eq "severity") {
                    my $severity = $va + 0;
                    Log::Nxlog::set_field_integer( $event, "NessusSeverityValue", $severity );
                    Log::Nxlog::set_field_string( $event, "NessusSeverity", $nessus_sev[$severity] );
                    Log::Nxlog::set_field_integer( $event, "SeverityValue", $nxlog_sev_val[$severity] );
                    Log::Nxlog::set_field_string( $event, "Severity", $nxlog_sev[$severity] );
                } else {
                    Log::Nxlog::set_field_string( $event, $na, $va );
                }
            }
            if ( $reportItem->hasChildNodes ) {
                my @kids = grep { $_->nodeType == XML_ELEMENT_NODE }
                  $reportItem->childNodes();
                foreach my $kid (@kids) {
                    my $na = $kid->getName();
                    my $va = "" . $reportItem->getChildrenByTagName($na);
                    Log::Nxlog::set_field_string( $event, $na, $va );
                }
            }
            Log::Nxlog::add_input_data($event);
        }
    }
}