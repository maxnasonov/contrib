#!/usr/bin/perl
use strict;
use warnings;
use Log::Nxlog;
use MIME::Base64;
use Digest::SHA qw(hmac_sha256);

BEGIN {
    # Statements to be executed when NXLog starts
}

# This subroutine is invoked in nxlog.conf as follows:
#
#    Exec  {instance_name}->call("genauth");
#
# where {instance_name} is the actual intance name defined for
# the xm_perl module, e.g.: perl->call("genauth");
#
sub genauth {
    my ($event) = @_;
    my $contentLength = Log::Nxlog::get_field($event,'ContentLength');
    my $x_ms_date = Log::Nxlog::get_field($event,'x_ms_date');
    my $workspace = Log::Nxlog::get_field($event,'Workspace');
    my $sharedKey = Log::Nxlog::get_field($event,'SharedKey');
    my $contentType = 'application/json';
    my $resource = '/api/logs';
    my $stringToSign = "POST\n"
      . $contentLength ."\n"
      . $contentType ."\n"
      . 'x-ms-date:'.$x_ms_date ."\n"
      . $resource;
    my $decodedSharedKey = decode_base64($sharedKey);
    my $hashedString = hmac_sha256($stringToSign,$decodedSharedKey);
    my $hashedStringBase64 = encode_base64($hashedString,"");
    my $authorization = "SharedKey $workspace:$hashedStringBase64";
    my $logMessage =
       "Generated from Shared Key and hashed signing string based on:
        ContentLength: $contentLength
        x-ms-date:     $x_ms_date
        Authorization: $authorization";
    Log::Nxlog::set_field_string($event,'Authorization',$authorization);
    Log::Nxlog::log_info($logMessage);
}
