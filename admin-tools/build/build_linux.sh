#!/usr/bin/env bash

######
# This script is to build nxlog-admin-tools on linux distributions
######

# exit on subshells exiting with 77
set -E
trap '[ "$?" -ne 77 ] || exit 1' ERR

#my_pyinstaller_bin=`which pyinstaller 2>/dev/null` || { echo "need pyinstaller to be installed, exiting"; exit 1; }
my_relative_path="`dirname \"$0\"`"
export MY_SCRIPT_PATH="`( cd \"${my_relative_path}\" && pwd )`"
export MY_NAME="nxlog-admin-tools"
export NXLOG_ADDON_PATH="/opt/$MY_NAME"
# not needed, we will build one binary
export NXLOG_ADDON_SO_PATH="$NXLOG_ADDON_PATH/data"

# exit if this is not a git repository
if ! git rev-parse --git-dir > /dev/null 2>&1; then
  echo "WARNING: This script should be run from a git repository"
fi

# get the version
# rhel6 has 1.7.1, which does not have rev-list --count
if [ ! -f ../VERSION ]; then
  echo "WARNING: No 'VERSION' file found"
  export MY_VERSION=`git log --pretty=oneline | wc -l`
else
  export MY_VERSION="`cat ../VERSION`.`git log --pretty=oneline | wc -l`"
fi
echo \$MY_VERSION: $MY_VERSION

if [ -f "/etc/redhat-release" ]; then
  #TODO: this is now hardcoded, find instead
  #export my_libperl_so_file="/opt/perl/perl5/perlbrew/build/perl-5.14.0/perl-5.14.0/libperl.so"
  #export my_libssl_so_file="/usr/lib64/libssl.so"
  #export my_libcrypto_so_file="/usr/lib64/libcrypto.so"
  #UTF_HEAVY="-M utf8_heavy.pl -M unicore/Heavy.pl"
  #export my_libossp_uuid_so_file="/usr/lib64/libossp-uuid.so.16"
  my_dist="rpm"
else
  #export my_libperl_so_file="/usr/lib/x86_64-linux-gnu/libperl.so"
  #export my_libssl_so_file="/usr/lib/x86_64-linux-gnu/libssl.so"
  #export my_libcrypto_so_file="/usr/lib/x86_64-linux-gnu/libcrypto.so"
  #export MY_MOZILLA_CA_CERT="/usr/local/share/perl/5.20.2/Mozilla/CA/cacert.pem"
  #export my_libossp_uuid_so_file="/usr/lib/x86_64-linux-gnu/libossp-uuid.so.16"
  my_dist="deb"
fi

#(
#echo """----
#Package python source with modules to one file
#----
#"""
#
#cd ../src
## package perl script with decrypt module
#if [ ! -f admin-tools.py ]; then
#  echo "ERROR: can not find source file, exiting"
#  exit 1
#fi
#${my_pyinstaller_bin} --distpath ../build/dist --workpath ../build/build -F --specpath ../build --clean --log-level INFO --key Tombola123 admin-tools.py || { echo "ERROR: could not package python script, exiting"; exit 77; }
#)

(
echo """
----
Generate documentation
----
"""

cd ../doc

# generate documentation
./generate-docs.sh || { echo "ERROR: could not generate document files, exiting"; exit 77; }
)

echo """
----
Build package
----
"""


if [ "${my_dist}" = "rpm" ]; then
  ./make_rpm.sh || { echo "ERROR: could not build package, exiting"; exit 1; }
elif [ "${my_dist}" = "deb" ]; then
  ./make_deb.sh || { echo "ERROR: could not build package, exiting"; exit 1; }
fi

echo """
----
Finished
----
"""

exit 0
