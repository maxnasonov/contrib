#!/usr/bin/env bash

rm -rf $HOME/rpmbuild

my_spec_file="nxlog-admin-tools.spec"

echo "# do not strip binaries
%global __os_install_post %{nil}

Name: ${MY_NAME}
Version: ${MY_VERSION}
Release: 1
Summary: Admin Tools addon for NXLog
License: Commercial
Group: System Environment/Daemons
Vendor: NXLog Ltd
URL: http://nxlog.org
BuildRoot: %(echo \$HOME)/%{name}-%{version}
# or is available only from RPM 4.13
#Requires(pre): nxlog or nxlog-trial
# require the nxlog binary for install
Requires(pre): /opt/nxlog/bin/nxlog
# require the nxlog binary all times
Requires: /opt/nxlog/bin/nxlog, python-enum34

%description
This package contains Admin Tools addon for NXLog.

%build

%install
mkdir -p \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}
cp -r $MY_SCRIPT_PATH/../src/* \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}
# todo: To copy the keys, remove next line if necessary
rm -f \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}/keys/*
chmod 0700 \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}/keys
rm -f \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}/var/*

find \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH} -type d -exec chmod 0755 \{\} \;
find \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH} -type f -exec chmod 0644 \{\} \;" > $my_spec_file

# create doc dir
echo """# install documentation
install -d -m 0755 \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}/doc/
cp -a \$MY_SCRIPT_PATH/../{copyright,license}.txt \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}/doc""" >> $my_spec_file

## install documentation if available
#echo "cp $MY_SCRIPT_PATH/static_files/README_lin.txt \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}/doc/README.txt" >> $my_spec_file
#if [ -d $MY_SCRIPT_PATH/../doc/output ] && ls $MY_SCRIPT_PATH/../doc/output/*; then
#  echo """cp -a $MY_SCRIPT_PATH/../doc/output/* \$RPM_BUILD_ROOT${NXLOG_ADDON_PATH}/doc/""" >> $my_spec_file
#fi

# add LICENSE if available
#if [ -f "${MY_SCRIPT_PATH}/../LICENSE" ]; then
#  echo """# install license
#cp $MY_SCRIPT_PATH/../LICENSE %{_topdir}/BUILD/""" >> $my_spec_file
#fi

echo """
%files
%defattr(-,root,root)
${NXLOG_ADDON_PATH}
%config ${NXLOG_ADDON_PATH}/nxlog-admin.conf.json
%dir ${NXLOG_ADDON_PATH}/var""" >> $my_spec_file

# add LICENSE if available
#if [ -f "${MY_SCRIPT_PATH}/../LICENSE" ]; then
#  echo """# can not use license
## https://bugzilla.redhat.com/show_bug.cgi?id=1386639
#%doc LICENSE
#""" >> $my_spec_file
#fi

echo """
%post
# chown conf and log directories the same as nxlog
BASE=${NXLOG_ADDON_PATH}
MY_USER=\`stat -c \"%U\" /opt/nxlog/var/log/nxlog\`
MY_GROUP=\`stat -c \"%G\" /opt/nxlog/var/log/nxlog\`
chown \${MY_USER}:\${MY_GROUP} \${BASE}/var

%postun
BASE=${NXLOG_ADDON_PATH}
if [ "\$1" -eq 0 ]; then
  # Package removal, not upgrade
  if [ -d \${BASE} ]; then
    if [ -f \${BASE}/var/nxlog-admin.pid ] && [ -r \${BASE}/var/nxlog-admin.pid ]; then
      kill \$(cat \${BASE}/var/nxlog-admin.pid) >/dev/null 2>&1
      sleep 1
    fi
    # Chown all root
    chown -R 0:0 \${BASE} >/dev/null 2>&1
    find \${BASE} -type f -a \( -name '*.log' -or -name '*.pyc' \) -delete >/dev/null 2>&1
    # Delete empty directories
    find \${BASE} -type d -empty -delete >/dev/null 2>&1
  fi
fi
""" >> $my_spec_file

rpmbuild -ba $my_spec_file || { echo "ERROR: rpmbuild failed, exiting"; exit 1; }

cp $HOME/rpmbuild/RPMS/*/* ../..
cp $my_spec_file ../../
