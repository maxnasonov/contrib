from errno import ENOENT

import json
import os


# default config file placement
CONFIG_FILE = 'nxlog-admin.conf.json'
LOG_FILE = 'nxlog-admin.log'
PID_FILE = 'nxlog-admin.pid'
DAEMON_LOG_FILE = 'daemon.log'
DAEMON_ERROR_LOG_FILE = 'daemon-err.log'



def json_load_comments(filename):

    file_data = open(filename, 'r')
    content = ''
    while True:
        line = file_data.readline()
        if not line:
            break
        line = line.lstrip()
        if not line.startswith('//'):
            content += line
    return json.loads(content)


class ConfError(Exception):

    def __init__(self, file, reason):
        self.file = file
        self.reason = reason

    def __str__(self):
        return "Error in config file '{}': {}".format(self.file, self.reason)


class Listen:

    def __init__(self, ip=None, port=None, proto=None):
        self.ip = ip
        self.port = port
        self.proto = proto
        self.ssl_cert = None
        self.ssl_key = None
        self.ssl_ca = None

    def __str__(self):
        return """{self.proto} {self.ip}:{self.port}
        key={self.ssl_key}
        cert={self.ssl_cert}
        ca={self.ssl_ca}""".format(self=self)

    def check_file(self, name):
        if name is None:
            return
        if not os.path.exists(name):
            raise IOError(ENOENT, 'File not found', name)
        if not os.path.isfile(name):
            raise IOError(ENOENT, 'Not a file', name)

    def check_files(self):
        self.check_file(self.ssl_ca)
        self.check_file(self.ssl_cert)
        self.check_file(self.ssl_key)


class CtlSocket:

    def __init__(self, file):
        self.file = file



class Agent:

    def __init__(self, host, agent_type, description=None, port=8080,
                 proto='tcp', ssl_ca=None, ssl_cert=None, ssl_key=None):
        self.type = agent_type
        self.host = host
        self.description = description
        self.port = port
        self.proto = proto
        self.ssl_ca = ssl_ca
        self.ssl_cert = ssl_cert
        self.ssl_key = ssl_key
        self.check_files()


    def check_file(self, name):
        if name is None:
            return
        if not os.path.exists(name):
            raise IOError(ENOENT, 'File not found', name)
        if not os.path.isfile(name):
            raise IOError(ENOENT, 'Not a file', name)

    def check_files(self):
        self.check_file(self.ssl_ca)
        self.check_file(self.ssl_cert)
        self.check_file(self.ssl_key)



class Conf():

    def __init__(self):
        self.root = os.getcwd()
        self.pidfile = os.path.join(self.root, 'var')
        self.listen = []
        self.ctls = []
        self.agents = []
        self.defines = {}
        self.daemon_log = ''
        self.daemon_err_log = ''

    def init_reload(self, filename=None):
        if filename is None:
            self.filename = CONFIG_FILE
        else:
            self.filename = filename
        if not os.path.isfile(self.filename):
            raise ConfError(self.filename, 'File is missing')

        self.reload()

    def make_listen(self, data):
        new_listen = Listen(data["ip"], data["port"], data["proto"])
        if new_listen.proto == "ssl":
            if 'ssl_ca' in data:
                new_listen.ssl_ca = self.var(data["ssl_ca"])
            if 'ssl_cert' in data:
                new_listen.ssl_cert = self.var(data["ssl_cert"])
            if 'ssl_key' in data:
                new_listen.ssl_key = self.var(data["ssl_key"])
        new_listen.check_files()
        return new_listen

    def make_agent(self, data):
        agent = Agent(data['host'], "passive", data['description'],
                      data['port'], data['proto'])
        if agent.proto == 'ssl':
            agent.ssl_key = self.var(data['ssl_key'])
            agent.ssl_cert = self.var(data['ssl_cert'])
            agent.ssl_ca = self.var(data['ssl_ca'])
            agent.check_files()

        return agent

    def var(self, value):
        for key, val in self.defines.iteritems():
            name = '{' + key + '}'
            if value.find(name) != -1:
                value = value.replace(name, val)
        return value

    def reload_vars(self, data):
        self.defines = data['defines']
        self.defines['cwd'] = os.path.abspath(os.path.curdir)
        for key, val in self.defines.iteritems():
            self.defines[key] = self.var(val)

    def reload_var(self, data, name, default):
        return self.var(data[name]) if name in data else default

    def reload_path(self, data):

        self.pidfile = self.reload_var(data, 'pidfile', PID_FILE)
        self.logfile = self.reload_var(data, 'logfile', LOG_FILE)
        self.daemon_log = self.reload_var(data, 'daemon-log', DAEMON_LOG_FILE)
        self.daemon_err_log = self.reload_var(data, 'daemon-err-log', DAEMON_ERROR_LOG_FILE)

    def reload(self):
        data = json_load_comments(self.filename)
        self.reload_vars(data)
        self.reload_path(data)

        self.listen = [self.make_listen(elem) for elem in data['listen']]
        sockets = [self.var(elem['file']) for elem in data['sockets']]
        self.ctls = [CtlSocket(self.var(file)) for file in sockets]
        self.agents = [self.make_agent(elem) for elem in data['agents']]

file = Conf()


def reload_init(filename):
    file.init_reload(filename)
