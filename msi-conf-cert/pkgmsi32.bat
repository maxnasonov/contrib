@echo off
REM Set the path for WiX
SET ProgramFilesPath=%ProgramFiles(x86)%
SET WIX_BUILD_LOCATION=%ProgramFilesPath%\WiX Toolset v3.11\bin
REM Set the architecture to either x86 or x64
SET PROCARCH=x86

REM Cleanup leftover intermediate files
del /f /q "*.wixobj" "*.wixpdb"

echo Building nxlog-conf_%PROCARCH%.msi
echo.
"%WIX_BUILD_LOCATION%\candle.exe" -dProcessorArchitecture=%PROCARCH% nxlog-conf.wxs
"%WIX_BUILD_LOCATION%\light.exe" -ext WixUIExtension nxlog-conf.wixobj -o "nxlog-conf_%PROCARCH%.msi"

REM Cleanup leftover intermediate files
del /f /q "*.wixobj" "*.wixpdb"